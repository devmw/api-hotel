from django.shortcuts import render
from django.http import HttpResponse, JsonResponse, QueryDict
from django.views.decorators.csrf import csrf_exempt
from django.forms.models import model_to_dict
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from datetime import date
from .serializers import VisitorSerializer, EmployeeSerializer, EventQuerySerializer, EventExitSerializer
from .models import Visitor, Employee
import json

class Home():
    @classmethod
    @csrf_exempt
    def index(cls, request):
        return HttpResponse("Try with other path.<br>visitor/<br>employee/", status=200)

class HomeVisitor(APIView):

    bad_access = HttpResponse("Bad access", status=400)
    not_record = HttpResponse("Not found records", status=400)
    not_exists = HttpResponse("Employee not found", status=400)
    respons_ok = JsonResponse({'msg': 'ok'}, status=200)

    def __init__(self):
        pass
        
    @classmethod
    @csrf_exempt
    def visitor(cls, request):
        if request.method == 'POST':
            payload = QueryDict(request.body)
            serializer = VisitorSerializer(data=payload)
            eventQuerySerializer = EventQuerySerializer(data=payload)
            eventExiterializer = EventExitSerializer(data=payload)

            if payload['event_type'] == 'consulta' and eventQuerySerializer.is_valid():
                data = [x for x in Visitor.objects.all().values()]
                return JsonResponse({'visitors': data}, status=200)
         
            if payload['event_type'] == 'salida_visitante' and eventExiterializer.is_valid():
                record = Visitor.objects.filter(document_number=payload['document_number']).last()
                if record:
                    try:
                        record.departure_time = payload['departure_time']
                        record.save()
                        return cls.respons_ok
                    except: pass
                else:
                    return cls.not_record

            if serializer.is_valid():
                document_exists = Employee.objects.filter(document_number=payload['document_number_employee']).exists()
                if payload['event_type'] == 'llegada_visitante' and document_exists:
                    try:
                        serializer.save()
                        return cls.respons_ok
                    except: pass
                else:
                    return cls.not_exists

        return cls.bad_access

    @classmethod
    @csrf_exempt
    def visitors(cls, request):
        if request.method == 'GET':
            data = [x for x in Visitor.objects.all().values()]
            return JsonResponse({'visitors': data}, status=200)
        
        return cls.bad_access


class HomeEmployee(APIView):

    bad_access = HttpResponse("Bad access", status=400)

    def __init__(self):
        pass

    @classmethod
    @csrf_exempt
    def employee(cls, request):
        if request.method == 'POST':
            payload = QueryDict(request.body)
            serializer = EmployeeSerializer(data=payload)
            if serializer.is_valid():
                serializer.save()
                return JsonResponse({'msg': 'ok'}, status=200)

        return cls.bad_access
    
    @classmethod
    @csrf_exempt
    def employees(cls, request):
        if request.method == 'GET':
            data = [model_to_dict(x) for x in Employee.objects.all()]
            return JsonResponse({'employees': data}, status=200)
        
        return cls.bad_access
