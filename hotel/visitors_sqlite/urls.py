from django.urls import path
from .views import HomeVisitor, HomeEmployee, Home

urlpatterns = [
    path('employee/', HomeEmployee.employee, name='exployee'),
    path('employees/', HomeEmployee.employees, name='exployees'),
    path('visitor/', HomeVisitor.visitor, name='visitor'),
    path('visitors/', HomeVisitor.visitors, name='visitors'),
    path('', Home.index, name='index'),
]
