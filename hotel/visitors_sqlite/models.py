from django.db import models


EVENT = (
    ('consulta', 'Consulta de Visitante'),
    ('llegada_visitante', 'Llegada del Visitante'),
    ('salida_visitante', 'Salida del Visitante'),
)

TYPE_DOCUMENT = (
    ('cc', 'Cedula de ciudadania'),
    ('nit', 'NIT'),
    ('ti', 'Tarjeta de identidad'),
)

class Visitor(models.Model):
    event_type = models.CharField(max_length=40, choices=EVENT)
    visitor_name = models.CharField(max_length=50)
    document_type = models.CharField(max_length=3, choices=TYPE_DOCUMENT)
    document_number = models.CharField(max_length=15)
    document_number_employee = models.CharField(max_length=15)
    arrive_time = models.DateField(auto_now=True, null=True)
    departure_time = models.DateField(auto_now=False, auto_now_add=False, null=True)


class Employee(models.Model):
    document_type = models.CharField(max_length=3, choices=TYPE_DOCUMENT)
    document_number = models.CharField(max_length=15)
