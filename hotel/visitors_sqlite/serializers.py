from rest_framework import serializers
from .models import Visitor, Employee

# 
# Visitor Serializers
# 
class VisitorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Visitor
        fields = ('event_type', 'visitor_name', 'document_type', 'document_number', 'document_number_employee',)

# Visitor Query Serializer
class EventQuerySerializer(serializers.ModelSerializer):
    class Meta:
        model = Visitor
        fields = ('event_type',)

# Visitor Exit Serializer
class EventExitSerializer(serializers.ModelSerializer):
    class Meta:
        model = Visitor
        fields = ('event_type', 'document_number', 'departure_time',)


# 
# Employee Serializers
# 
class EmployeeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Employee
        fields = ('document_type', 'document_number',)