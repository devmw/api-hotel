# Generated by Django 3.0.2 on 2020-01-19 05:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('visitors_sqlite', '0004_auto_20200119_0504'),
    ]

    operations = [
        migrations.AddField(
            model_name='visitor',
            name='document_number_employee',
            field=models.CharField(default='', max_length=15),
        ),
        migrations.AlterField(
            model_name='visitor',
            name='arrival_time',
            field=models.DateField(auto_now=True, null=True),
        ),
        migrations.AlterField(
            model_name='visitor',
            name='departure_time',
            field=models.DateField(null=True),
        ),
        migrations.AlterField(
            model_name='visitor',
            name='document_number',
            field=models.CharField(default='', max_length=15),
        ),
    ]
