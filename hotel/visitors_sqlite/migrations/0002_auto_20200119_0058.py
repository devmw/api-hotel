# Generated by Django 3.0.2 on 2020-01-19 00:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('visitors_sqlite', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='employee',
            name='document_type',
            field=models.CharField(choices=[(1, 'CC'), (2, 'NIT'), (3, 'TI')], max_length=3),
        ),
        migrations.AlterField(
            model_name='visitor',
            name='document_type',
            field=models.CharField(choices=[(1, 'CC'), (2, 'NIT'), (3, 'TI')], max_length=3),
        ),
    ]
