from django.apps import AppConfig


class VisitorsSqliteConfig(AppConfig):
    name = 'visitors_sqlite'
