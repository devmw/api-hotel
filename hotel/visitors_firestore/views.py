from django.shortcuts import render
from django.http import HttpResponse, JsonResponse, QueryDict
from django.views.decorators.csrf import csrf_exempt
from firebase_admin import credentials
from firebase_admin import firestore
from datetime import date
from .args import Args
import firebase_admin
import json, os

cred = credentials.Certificate(os.environ['GOOGLE_APPLICATION_CREDENTIALS'])
firebase_admin.initialize_app(cred)
db = firestore.client()

class Home():
    @classmethod
    @csrf_exempt
    def index(cls, request):
        return HttpResponse('Try with other path.<br>visitor/<br>employee/', status=200)

class HomeVisitor():

    # Response
    bad_access = HttpResponse('Bad access', status=400)
    not_record = HttpResponse('Not found records', status=400)
    not_exists_employee = HttpResponse('Employee not found or invalid document type', status=400)
    not_exists_visitor = HttpResponse('Visitor not found', status=400)
    respons_ok = JsonResponse({'msg': 'ok'}, status=200)

    events = [x for x in Args.getEvent()]
    args_a = [x for x in Args.getArrive()]
    args_d = [x for x in Args.getDeparture()]
    doc_ty = [x for x in Args.getDocumentTypes()]

    typeUser1 = 'visitor'
    typeUser2 = 'employee'

    today = date.today()
    today.strftime("%Y-%m-%d")

    def __init__(self):
        pass
        
    @classmethod
    @csrf_exempt
    def visitor(cls, request):
        if request.method == 'POST':
            payload = QueryDict(request.body)
            parameters = [x for x in payload]
            
            if 'event_type' in parameters:
                if payload['event_type'] == cls.events[0]:
                    data = cls.listVisitor()
                    return JsonResponse({'visitors': data}, status=200)

                elif payload['event_type'] == cls.events[1] and parameters == cls.args_a:
                    if cls.existsUser(cls.typeUser2, payload[cls.args_a[4]]) and payload['document_type'] in cls.doc_ty:
                        if payload['document_type'] and cls.createVisitor(payload['document_number'], payload):
                            return cls.respons_ok
                    else:
                        return cls.not_exists_employee

                elif payload['event_type'] == cls.events[2] and parameters == cls.args_d:
                    if cls.existsUser(cls.typeUser1, payload[cls.args_d[1]]):
                        if payload['document_number'] and cls.updateVisitor(payload['document_number'], payload):
                            return cls.respons_ok
                    else:
                        return cls.not_exists_visitor
                else:
                    pass

        return cls.bad_access

    @classmethod
    @csrf_exempt
    def visitors(cls, request):
        if request.method == 'GET':
            data = cls.listVisitor()
            return JsonResponse({'visitors': data}, status=200)
        
        return cls.bad_access

    @classmethod
    def listVisitor(cls):
        users_ref = db.collection(u'visitor')
        docs = users_ref.stream()
        data = [x.to_dict() for x in docs]

        return data

    @classmethod
    def createVisitor(cls, document_number='', payload=[]):
        try:
            doc_ref = db.collection(u'visitor').document(u'{dc}'.format(dc=document_number))
            doc_ref.set({
                u'visitor_name': u'{arg}'.format(arg=payload['visitor_name']),
                u'document_type': u'{arg}'.format(arg=payload['document_type']),
                u'document_number': u'{arg}'.format(arg=payload['document_number']),
                u'document_number_employee': u'{arg}'.format(arg=payload['document_number_employee']),
                u'arrive_time': u'{arg}'.format(arg=cls.today)
            })
            return True
        except:
            return False

    @classmethod
    def updateVisitor(cls, document_number='', payload=[]):
        try:
            doc_ref = db.collection(u'visitor').document(u'{dc}'.format(dc=document_number))
            doc_ref.update({
                u'departure_time': u'{arg}'.format(arg=payload['departure_time']),
            })
            return True
        except:
            return False
    
    @classmethod
    def existsUser(cls, type='', documentVisitor=''):
        doc_ref = db.collection(u'{type}'.format(type=type)).document(u'{dc}'.format(dc=documentVisitor))
        try:
            doc = doc_ref.get()
            if doc.to_dict() == None:
                return False
            else:
                return True
        except:
            return False



class HomeEmployee():

    bad_access = HttpResponse("Bad access", status=400)
    args = [x for x in Args.getEmployee()]

    def __init__(self):
        pass

    @classmethod
    @csrf_exempt
    def employee(cls, request):
        if request.method == 'POST':
            payload = QueryDict(request.body)
            parameters = [x for x in payload]
            if cls.args == parameters:
                doc_ref = db.collection(u'employee').document(u'{dn}'.format(dn=payload[cls.args[1]]))
                doc_ref.set({
                    u'document_type': u'{dt}'.format(dt=payload[cls.args[0]]),
                    u'document_number': u'{dn}'.format(dn=payload[cls.args[1]]),
                })
                return JsonResponse({'msg': 'ok'}, status=200)
    
        return cls.bad_access
    
    @classmethod
    @csrf_exempt
    def employees(cls, request):
        if request.method == 'GET':
            users_ref = db.collection(u'employee')
            docs = users_ref.stream()
            data = [x.to_dict() for x in docs]
            return JsonResponse({'employees': data}, status=200)
        
        return cls.bad_access
