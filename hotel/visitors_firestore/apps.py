from django.apps import AppConfig


class VisitorsFirestoreConfig(AppConfig):
    name = 'visitors_firestore'
