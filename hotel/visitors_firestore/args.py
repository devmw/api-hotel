class Args():
    events = (
        'consulta',
        'llegada_visitante', 
        'salida_visitante',
    )
    args_arrive = (
        'event_type',
        'visitor_name',
        'document_type', 
        'document_number',
        'document_number_employee',
    )
    args_departure = (
        'event_type', 
        'document_number',
        'departure_time',
    )
    document_types = (
        'cc',
        'nit',
        'ti',
    )
    args_employee = (
        'document_type', 
        'document_number',
    )
    

    def __init__(self):
        pass
    
    @classmethod
    def getEvent(cls):
        return cls.events

    @classmethod
    def getArrive(cls):
        return cls.args_arrive

    @classmethod
    def getDeparture(cls):
        return cls.args_departure
    
    @classmethod
    def getDocumentTypes(cls):
        return cls.document_types

    @classmethod
    def getEmployee(cls):
        return cls.args_employee